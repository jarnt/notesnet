#!/bin/sh

WEB_ROOT=/usr/share/nginx/html
HOSTNAME=$(hostname)

echo "The directory ${WEB_ROOT} is not mounted."
echo "Over-writing the default index.html file with some useful information."

# CONTAINER_IP=$(ip addr show eth0 | grep -w inet| awk '{print $2}')
# Note:
#   CONTAINER IP cannot always be on device 'eth0'.
#     It could be something else too, as pointed by @arnaudveron .
#   The 'ip -j route' shows JSON output,
#     and always shows the default route as the first entry.
#     It also shows the correct device name as 'prefsrc', with correct IP address.
CONTAINER_IP=$(ip -j route get 1 | jq -r '.[0] .prefsrc')

# Reduced the information in just one line. It overwrites the default text.
echo -e "Praqma Network MultiTool (with NGINX) - ${HOSTNAME} - ${CONTAINER_IP}" > ${WEB_ROOT}/index.html

# Execute the command specified as CMD in Dockerfile:
exec "$@"
